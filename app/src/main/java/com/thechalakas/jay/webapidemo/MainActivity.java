package com.thechalakas.jay.webapidemo;

/*
 * Created by jay on 13/09/17. 11:35 AM
 * https://www.linkedin.com/in/thesanguinetrainer/
 */

//references used
//http://www.techrepublic.com/blog/software-engineer/calling-restful-services-from-your-android-app/
//http://www.androidauthority.com/use-remote-web-api-within-android-app-617869/

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity
{

    //get the button handle

    Button button_call_api_1;

    //get the textview_api_results text handle

    TextView textview_api_results;

    //get the progress bar handle
    //Lets get the progress bar shall we?
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("MainActivity","onCreate Reached Start");

        //get the button handle

        button_call_api_1 = (Button) findViewById(R.id.button_call_api_1);

        //get the textview_api_results text handle

        textview_api_results = (TextView) findViewById(R.id.textview_api_results);
        //use this to make the text scrollable.
        //also on the xml page put this
        // android:scrollbars="vertical"
        textview_api_results.setMovementMethod(new ScrollingMovementMethod());

        //get the progress bar handle
        //Lets get the progress bar shall we?
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        //make progress bar invisible
        progressBar.setVisibility(View.GONE);

        button_call_api_1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                String msg = "button_call_api_1 setOnClickListener reached";
                Log.i("Button Clicks",msg);
                //variable used to show the text on the app
                //String textview_api_results_msg = "json stuff should come here";

                //alright, lets call the API
                ConsumeAPI2 consumeapi2 = new ConsumeAPI2();
                consumeapi2.execute();

            }
        });

        Log.i("MainActivity","onCreate Reached End");
    }


    //class that will be used to make the web api call
    class ConsumeAPI2 extends AsyncTask<Void,Void,String>
    {


        //make the progress bar appear, to indicate that we have begun the web call
        protected void onPreExecute()
        {
            progressBar.setVisibility(View.VISIBLE);
        }

        //do the actual web call and return the results
        @Override
        protected String doInBackground(Void... urls)
        {
            try
            {
                //your HTTP API address here. you can put any URL with any combination (like api KEY, php, and so on)
                //this is an API that I built for testing purposes which returns a collection of headphones.
                String API_URL="http://simplewebapi1webapp1.azurewebsites.net/api/Headphones";
                //a string wont work on android directly even if it looks like a URL you need to use a URL object
                URL url = new URL(API_URL);

                //following code is boiler plate. remains same almost everytime
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                try {
                    //the buffer reader will get the stream of data from the internet
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    //you then use a string builder to build the actual string that will take the stuff from the buffer reader
                    //and then get the string which has all the URL response
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    //read one line at a time from the buffer
                    while ((line = bufferedReader.readLine()) != null)
                    {
                        //and add it to the string that you are building
                        stringBuilder.append(line).append("\n");
                    }
                    //at this point the string is over. close the buffer object
                    bufferedReader.close();
                    //return the string that you have built which contains the full string response
                    return stringBuilder.toString();
                }
                //when the network call is over disconnect from the URL connection
                finally
                {
                    urlConnection.disconnect();
                }
            }
            //if something went wrong, catch the exception and show a simple error
            catch(Exception e)
            {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        //make the progress bar go away. the web call is completed.
        protected void onPostExecute(String response)
        {

            Log.i("MainActivity","onPostExecute Reached Start");
            progressBar.setVisibility(View.GONE);

            //do something with the response
            String msg = response;
            textview_api_results.setText(msg);

            Log.i("MainActivity","onPostExecute Reached End");
        }
    }
}
